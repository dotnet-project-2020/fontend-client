import {v4 as uuidv4} from 'uuid';

export function setCartId(id) {
  if (process.server) {
    return
  }
  window.localStorage.setItem('cartId', id)
}

export function getCartId() {
  if (process.server) {
    return
  }
  const cartId = localStorage.getItem('cartId');
  if (!cartId) setCartId(uuidv4())
  return localStorage.getItem('cartId');
}
