export function numberToMoney(number) {
  if (number && +number) {
    return number.toLocaleString('vi');
  }
  if (+number === 0) return +number
  return null;
}
