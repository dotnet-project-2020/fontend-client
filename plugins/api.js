import ApiProducts from '~/apis/ApiProduct'

export default ({$axios}, inject) => {
  const api  = {
    products: ApiProducts($axios)
  }

  // Inject `api` key
  // -> app.$api
  // -> this.$api in vue components
  // -> this.$api in store actions/mutations
  inject("api", api)
}
