const PATH = "products";

export default $axios => ({
  index() {
    return $axios.$get(PATH)
  },

  productsWithTypeTop(id) {
    return $axios.$get(PATH,
      {
        params: {
          typeId: id,
          perPage: 10
        }
      }
    )
  },

  productsWithType(id, query) {
    return $axios.$get(PATH,
      {
        params: {
          typeId: id,
          page: query?.page,
          perPage: query?.perPage
        }
      }
    )
  },

  show(id) {
    return $axios.$get(`${PATH}/${id}`)
  },

  search(key) {
    return $axios.$get(PATH, {
      params: {
        search: key
      }
    });
  },
})
