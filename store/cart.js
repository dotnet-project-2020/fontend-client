function plusQuantity(theItem, quantityAdd) {
  let quantity = theItem.cartItemQuantity;
  if (quantity + quantityAdd <= theItem.quantity) {
    quantity += quantityAdd
  }
  return quantity
}

function enterQuantity(theItem, quantity) {
  if (quantity <= theItem.quantity && quantity > 0) {
    return quantity
  }
  if (quantity > theItem.quantity) {
    return theItem.quantity
  }
  return 1
}

function removeQuantity(theItem, quantityAdd) {
  let quantity = theItem.cartItemQuantity;
  if (quantity - quantityAdd > 0) {
    quantity -= quantityAdd
  }
  return quantity
}

// CART ITEM STRUCTURE
// productId: this.theProduct.productId,
//   productName: this.theProduct.name,
//   productPictureUrl: this.theProduct.pictureUrl,
//   price: this.price,
//   originPrice: this.originPrice,
//   attributeValue: 'Xanh',
//   attributeValueSecond: '4 Gb',
//   id: theModel.id,
//   quantity: theModel.quantity,
//   cartItemQuantity: this.quantity,
//   cartItemStatus: true

export const state = () => ({
  id: 'id',
  items: []
})

export const mutations = {
  SET_CART_ID(state, data) {
    state.id = data
  },
  ADD_CART_ITEM(state, data) {
    state.items = state.items.filter(function (element) {
      return element !== undefined;
    })

    if (!state.items) {
      state.items = []
    }

    let theItem
    for (let i = 0; i < state.items.length; i++) {
      if (state.items[i].id === data.id) {
        theItem = state.items[i];
        break
      }
    }
    // if already contain item
    if (theItem) {
      const quantityAdd = data.cartItemQuantity
      theItem.cartItemQuantity = plusQuantity(theItem, quantityAdd);
    } else {
      // add new
      state.items.push(data)
    }
  },
  ADD_ITEM_QUANTITY(state, {itemId, quantity}) {
    let theItem
    for (let i = 0; i < state.items.length; i++) {
      if (state.items[i].id === itemId) {
        theItem = state.items[i];
        break
      }
    }
    // if already contain item
    if (theItem) {
      theItem.cartItemQuantity = plusQuantity(theItem, quantity);
    }
  },
  ENTER_ITEM_QUANTITY(state, {itemId, quantity}) {
    let theItem
    for (let i = 0; i < state.items.length; i++) {
      if (state.items[i].id === itemId) {
        theItem = state.items[i];
        break
      }
    }
    // if already contain item
    if (theItem) {
      theItem.cartItemQuantity = enterQuantity(theItem, quantity);
    }
  },
  REMOVE_ITEM_QUANTITY(state, {itemId, quantity}) {
    let theItem
    for (let i = 0; i < state.items.length; i++) {
      if (state.items[i].id === itemId) {
        theItem = state.items[i];
        break
      }
    }
    // if already contain item
    if (theItem) {
      theItem.cartItemQuantity = removeQuantity(theItem, quantity);
    }
  },
  CHANGE_ITEM_STATUS(state, {itemId, status}) {
    let theItem
    for (let i = 0; i < state.items.length; i++) {
      if (state.items[i].id === itemId) {
        theItem = state.items[i];
        break
      }
    }
    // if already contain item
    if (theItem) {
      theItem.status = status
    }
  }
}

export const actions = {
  addItem({commit, dispatch}, object) {
    commit('ADD_CART_ITEM', object)
    dispatch('showAlert', {
      status: true,
      content: 'Thêm sản phẩm vào giỏ hàng thành công',
      color: 'success'
    }, {root: true})
  },
  addItemQuantity({commit, dispatch}, {itemId, quantity}) {
    commit('ADD_ITEM_QUANTITY', {itemId, quantity})
    dispatch('showAlert', {
      status: true,
      content: 'Thêm số lượng thành công',
      color: 'success'
    }, {root: true})
  },
  enterItemQuantity({commit, dispatch}, {itemId, quantity}) {
    commit('ENTER_ITEM_QUANTITY', {itemId, quantity})
    dispatch('showAlert', {
      status: true,
      content: 'Thay đổi số lượng thành công',
      color: 'success'
    }, {root: true})
  },
  removeItemQuantity({commit, dispatch}, {itemId, quantity}) {
    commit('REMOVE_ITEM_QUANTITY', {itemId, quantity})
    dispatch('showAlert', {
      status: true,
      content: 'Giảm số lượng thành công',
      color: 'success'
    }, {root: true})
  },
  changeItemStatus({commit, dispatch}, {itemId, status}) {
    commit('CHANGE_ITEM_STATUS', {itemId, status})
    dispatch('showAlert', {
      status: true,
      content: 'Thay đổi trạng thái thành công',
      color: 'success'
    }, {root: true})
  }
}

export const getters = {
  cartProducts(state) {
    return state.items.map((item) => ({
      ...item,
      name: item.productName,
      pictureUrl: item.productPictureUrl,
      id: item.id,
      price: item.price,
      originPrice: item.originPrice,
      quantity: item.cartItemQuantity,
      status: item.cartItemStatus
    }))
  },

  cartTotalPrice(state) {
    let sum = 0
    state.items.forEach(item => {
      if (item.cartItemStatus === true) {
        sum += item.price * item.cartItemQuantity
      }
    })
    return sum
  },

  cartTotalItem(state) {
    let total = 0;
    state.items.forEach(item => {
      if (item && item.cartItemQuantity) {
        total += item.cartItemQuantity
      }
    })
    return total
  }
}
