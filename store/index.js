export const state = () => ({
  showSnackBar: false,
  snackBarContent: '',
  snackBarColor: 'success'
})
export const mutations = {
  SET_DISPLAY_SNACKBAR(state, data) {
    state.showSnackBar = data
  },
  SET_SNACKBAR_CONTENT(state, data) {
    state.snackBarContent = data
  },
  SET_SNACKBAR_COLOR(state, data) {
    state.snackBarColor = data
  }
}

export const actions = {
  showAlert({commit}, {status, content, color}) {
    commit('SET_DISPLAY_SNACKBAR', status)
    commit('SET_SNACKBAR_CONTENT', content)
    commit('SET_SNACKBAR_COLOR', color)
  }
}

export const getters = {
  showSnackBar(state) {
    return state.showSnackBar
  },
  snackBarContent(state) {
    return state.snackBarContent
  },
  snackBarColor(state) {
    return state.snackBarColor
  }
}
